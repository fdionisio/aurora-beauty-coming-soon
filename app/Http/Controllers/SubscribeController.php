<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Newsletter;

class SubscribeController extends Controller
{
    
    public function index(Request $request)
    {
        $validatedData = $request->validate([
            'email' => 'required|email|max:255',
        ]);
    
        if ( ! Newsletter::isSubscribed($request->post('email')) ) {
            Newsletter::subscribe($request->post('email'));
        }
        else{
            return back()->with('error', "You've already subscribed to our newsletter.");
        }

        return back()->with("success", "Great! You've successfully subscribed to our newsletter.");
    }

}
