<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Aurora Beauty</title>

        <!-- Favicon -->
        <link rel="shortcut icon" type="image/png" href="{{ asset('images/favicon-96x96.png') }}"/>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Work+Sans&display=swap" rel="stylesheet">
        <!-- CSS -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <!-- UIkit CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.5/dist/css/uikit.min.css" />
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-7NEN3V01EH"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-7NEN3V01EH');
        </script>
    </head>
    <body>
        <div class="uk-container-expand">
            <!-- Nav Section -->
            <div class="uk-child-width-expand@s uk-text-center nav-container" uk-grid>
                <div></div>
                <div>
                    <div class="uk-card uk-card-body">
                        <a href="" class="uk-navbar-item uk-logo">
                            <img class="logo" src="{{ asset('images/Aurora-Logo-variation-black.png') }}">
                        </a>
                    </div>
                </div>
                <div></div>
            </div>
            <div class="uk-navbar-center">
                
            </div>
            <div class="uk-container">
                <div class="uk-child-width-expand@s" uk-grid>
                    <div class="uk-width-auto@m main-txt">
                        <div class="uk-padding-top">
                            <h1>BRB, getting a makeover!</h1>
                            <p>
                                We'll be ready for your glow up soon.
                            </p>
                            <form class="subscribe-form" method="POST" action="{{ url('/subscribe') }} ">
                                @csrf
                                <div class="uk-margin">
                                    <div class="uk-inline">
                                        <button type="submit" class="uk-form-icon uk-form-icon-flip uk-button notify-btn" href="#">Notify Me</button>
                                        <input class="uk-input" type="text" name="email" placeholder="Enter your e-mail">
                                        <!-- <button type="submit" class="btn notify-btn">Notify Me</button> -->
                                    </div>
                                </div>
                                @error('email')
                                    <div class="uk-alert-danger" uk-alert>
                                        {{ $message }}
                                    </div>
                                @enderror
                                @if(session('error'))
                                    <div class="uk-alert-danger" uk-alert>
                                        {{ session('error') }}
                                    </div>
                                @endif
                                @if(session('success'))
                                    <div class="uk-alert-success" uk-alert>
                                        {{ session('success') }}
                                    </div>
                                @endif
                            </form>
                            <div class="social uk-flex-inline">
                                <!-- Facebook Icon -->
                                <a class="uk-margin-right" href="https://www.facebook.com/Aurora-Beauty-105026864504858" target="_blank">
                                    <div>
                                        <svg class="social-icon" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512" xml:space="preserve">
                                        <g>
                                            <g>
                                                <path d="M288,176v-64c0-17.7,14.3-32,32-32h32V0h-64c-53,0-96,43-96,96v80h-64v80h64v256h96V256h64l32-80H288z"/>
                                            </g>
                                        </g>
                                        </svg>
                                    </div>
                                </a>
                                <!-- Twitter Icon -->
                                <a class="uk-margin-right" href="https://twitter.com/auroraglowup" target="_blank">
                                    <div>
                                        <svg class="social-icon" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                        <g>
                                            <g>
                                                <path d="M512,97.2c-19,8.4-39.3,13.9-60.5,16.6c21.8-13,38.4-33.4,46.2-58c-20.3,12.1-42.7,20.6-66.6,25.4
                                                    C411.9,60.7,384.4,48,354.5,48c-58.1,0-104.9,47.2-104.9,105c0,8.3,0.7,16.3,2.4,23.9c-87.3-4.3-164.5-46.1-216.4-109.8
                                                    c-9.1,15.7-14.4,33.7-14.4,53.1c0,36.4,18.7,68.6,46.6,87.2c-16.9-0.3-33.4-5.2-47.4-12.9c0,0.3,0,0.7,0,1.2
                                                    c0,51,36.4,93.4,84.1,103.1c-8.5,2.3-17.9,3.5-27.5,3.5c-6.7,0-13.5-0.4-19.9-1.8c13.6,41.6,52.2,72.1,98.1,73.1
                                                    c-35.7,27.9-81.1,44.8-130.1,44.8c-8.6,0-16.9-0.4-25.1-1.4c46.5,30,101.6,47.1,161,47.1c193.2,0,298.8-160,298.8-298.7
                                                    c0-4.6-0.2-9.1-0.4-13.6C480.2,137,497.7,118.5,512,97.2z"/>
                                            </g>
                                        </g>
                                        </svg>
                                    </div>
                                </a>
                                <!-- Instagram -->
                                <a class="uk-margin-right" href="https://www.instagram.com/auroraglowup/" target="_blank">
                                    <div>
                                        <svg class="social-icon" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                        <g>
                                            <g>
                                                <path d="M352,0H160C71.6,0,0,71.6,0,160v192c0,88.4,71.6,160,160,160h192c88.4,0,160-71.6,160-160V160C512,71.6,440.4,0,352,0z
                                                    M464,352c0,61.8-50.2,112-112,112H160c-61.8,0-112-50.2-112-112V160C48,98.2,98.2,48,160,48h192c61.8,0,112,50.2,112,112V352z"/>
                                            </g>
                                        </g>
                                        <g>
                                            <g>
                                                <path d="M256,128c-70.7,0-128,57.3-128,128s57.3,128,128,128s128-57.3,128-128S326.7,128,256,128z M256,336c-44.1,0-80-35.9-80-80
                                                    c0-44.1,35.9-80,80-80s80,35.9,80,80C336,300.1,300.1,336,256,336z"/>
                                            </g>
                                        </g>
                                        <g>
                                            <g>
                                                <circle cx="393.6" cy="118.4" r="17.1"/>
                                            </g>
                                        </g>
                                        </svg>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="uk-width-expand@m coming-soon-face">
                        <div class="uk-text-center">
                            <img class="face-img" src="{{ asset('images/coming-soon-face.png')}}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <!-- UIkit JS -->
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.5/dist/js/uikit.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.5/dist/js/uikit-icons.min.js"></script>
</html>
